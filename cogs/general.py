import discord,logging,datetime,asyncio
from discord.ext import commands
from config.settings import settings
from cogs.utils import timeparser,formats


logging.basicConfig(filename="002.logs",level=logging.INFO,format='%(asctime)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

class General():
	def __init__(self,bot):
		self.bot = bot
		self.em = discord.Embed()

	@commands.command(pass_context=True,name="ping",help="Pings Zero Two.")
	async def _ping(self,context):
		now = datetime.datetime.utcnow()
		delta = now - context.message.timestamp
		latency = delta.microseconds / 1000
		username = context.message.author
		userId = username.id
		self.em.description = "<@{}> I'm here, Lu Lu! {}ms".format(userId,round(latency))
		await context.send(embed=self.em)
		logging.info("{}: ping - {}".format(username,latency))

	@commands.command(name="darling",help="Get invited to Zero Two's home.")
	async def _home(self,context):
		username = context.message.author
		userId = username.id
#		self.em.set_image(url="https://i.imgur.com/ERrUlUD.jpg")
		self.em.description = "<@{}> You want to join me Darling? Okay! I PM'd you an invite.".format(userId)
		await username.send("https://discord.gg/uG7djuh")
		await conext.send(embed=self.em)
		logging.info("{}: home".format(username))

	@commands.command(name="info",help="Information on Zero Two.")
	async def _info(self,context):
		github = "https://github.com/MustyMouse/ZeroTwo/"
		invite_link = settings["invite-link"]
		help_guild_link = "http://weeb.center"
		donate_link = "https://www.patreon.com/weebcenter"
		guildCount = len(self.bot.guilds)
		userCount = sum([len(guild.members) for guild in self.bot.guilds])
		info = await self.bot.application_info()
		em = discord.Embed(description=info.description,color=discord.Color.green())
		em.add_field(name="Author",value="<@{}>".format(info.owner.id))
		em.add_field(name="Bot ID",value=info.id)
		em.add_field(name="Guild count",value=guildCount)
		em.add_field(name="User count",value=userCount)
		em.add_field(name="Help", value='''
If you want to invite Zero Two to your server/guild, click this [invite link]({}).
If you have a question, suggestion, or just want to try out mah features, check out the [Help Server/Guild]({}).'''.format(invite_link,help_guild_link))
		em.add_field(name="Donate",value="[Become a patron]({}) and earn additional benefits.".format(donate_link))
		owner = (await self.bot.application_info()).owner
		em.set_author(name=self.bot.user.name, icon_url=self.bot.user.avatar_url, url=github)
		em.set_footer(text="002 developed by {}".format(owner.name), icon_url=owner.avatar_url)
		await context.send(embed=em)

	@commands.command(name="invite",help="Invite Zero Two to your server.")
	async def _invite(self,context):
		invite_link = settings["invite-link"]
		em = discord.Embed()
		em.set_author(name=self.bot.user.name, icon_url=self.bot.user.avatar_url, url=invite_link)
		em.color = discord.Color.blue()
		em.description = "[Invite me to your guild!]({})".format(invite_link)
		await context.send(embed=em)

	@commands.command(name="uptime",help="Get bot uptime.")
	async def _uptime(self,context):
		since = self.bot.uptime.strftime("%Y-%m-%d %H:%M:%S")
		passed = self.get_bot_uptime()
		await context.send("Uptime: **{}** (since {} UTC)"
							"".format(passed, since))

	def get_bot_uptime(self, *, brief=False):
		# Courtesy of Danny
		now = datetime.datetime.utcnow()
		delta = now - self.bot.uptime
		hours, remainder = divmod(int(delta.total_seconds()), 3600)
		minutes, seconds = divmod(remainder, 60)
		days, hours = divmod(hours, 24)

		if not brief:
			if days:
				fmt = '{d} days, {h} hours, {m} minutes, and {s} seconds'
			else:
				fmt = '{h} hours, {m} minutes, and {s} seconds'
		else:
			fmt = '{h}h {m}m {s}s'
			if days:
				fmt = '{d}d ' + fmt

		return fmt.format(d=days, h=hours, m=minutes, s=seconds)

	@commands.command(name="rage",help="Rages out and flips a table like Jesus.")
	async def _rage(self,context):
		first = '(╯°□°）╯︵ ┬─┬'
		second = '(╯°□°）╯︵ ┻━┻'
		third = '( ノ゜-゜)ノ ┬─┬'
		tmp = await context.send(first)
		await asyncio.sleep(0.75)
		tmp2 = await tmp.edit(content=second)
		await asyncio.sleep(0.75)
		tmp3 = await tmp.edit(content=first)
		await asyncio.sleep(0.75)
		await tmp.edit(content=third)

	@commands.command(name="remind",aliases=['timer', 'reminder'],help="Reminds you of things.")
	async def _remind(self, context, time : timeparser.TimeParser, *, message=''):
		"""
		Shamelessly stolen from 
			https://raw.githubusercontent.com/Rapptz/RoboDanny/b513a32dfbd4fdbd910f7f56d88d1d012ab44826/cogs/utils/formats.py
			https://raw.githubusercontent.com/Rapptz/RoboDanny/b513a32dfbd4fdbd910f7f56d88d1d012ab44826/cogs/meta.py
		"""
		author = context.message.author
		reminder = None
		completed = None
		message = message.replace('@everyone', '@\u200beveryone').replace('@here', '@\u200bhere')

		if not message:
			reminder = 'Okay {0.mention}, I\'ll remind you in {1}.'
			completed = 'Time is up {0.mention}! You asked to be reminded about something {2}.'
		else:
			reminder = 'Okay {0.mention}, I\'ll remind you about "{2}" in {1}.'
			completed = 'Time is up {0.mention}! You asked to be reminded about "{1}" {2}.'

		human_time = datetime.datetime.utcnow() - datetime.timedelta(seconds=time.seconds)
		human_time = formats.human_timedelta(human_time)
		await context.send(reminder.format(author, human_time.replace(' ago', ''), message))
		await asyncio.sleep(time.seconds)
		await context.send(completed.format(author, message, human_time))

	@_remind.error
	async def timer_error(self, error, context):
		if isinstance(error, commands.BadArgument):
			await context.send(str(error))

def setup(bot):
	bot.add_cog(General(bot))
