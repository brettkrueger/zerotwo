import discord,logging,datetime,sys
from discord.ext import commands

class Patrons:
	def __init__(self, bot):
		self.bot = bot

	@commands.command(name="testing")
	@commands.has_role("Patron")
	async def _testing(self,context):
		await context.send("Testing")

def setup(bot):
	bot.add_cog(Patrons(bot))
