import re,requests,socket,logging,asyncio
import discord
from discord.ext import commands

logging.basicConfig(filename="002.logs",level=logging.INFO,format='%(asctime)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

class Admin():
	def __init__(self,bot):
		self.bot = bot

	@commands.command(name="purge",aliases=['prune',"del"],help="Purge channel of messages.")
	@commands.has_permissions(ban_members = True)
	@commands.bot_has_permissions(manage_messages = True)
	async def _purge(self, context, *limit):
		try:
			limit = int(limit[0])
		except IndexError:
			limit = 1
		deleted = 0
		while limit >= 1:
			cap = min(limit, 100)
			deleted += len(await context.channel.purge(limit=cap, before=context.message))
			limit -= cap
		tmp = await context.send('**:put_litter_in_its_place:** {} messages deleted!'.format(deleted))
		await asyncio.sleep(15)
		await tmp.delete()
		await context.message.delete()

	@commands.command(name="kick",help="Kicks a user from the server.",aliases=["Kick"])
	@commands.has_permissions(kick_members=True)
	@commands.bot_has_permissions(kick_members = True)
	async def _kick(self,context,user: discord.Member=None, *reason):
		server = context.message.guild
		username = context.message.author
		em = discord.Embed()
		em.title = "Kick {}".format(user)
		if reason:
			reason = ' '.join(reason)
		else: reason = None
		if not isinstance(user,int):
			if not isinstance(user.name.partition("#")[1],int):
				em.description = "You need to be more specific than that, Darling!"
			else: user = server.get_member_named(user)

		try:
			await user.kick(reason=reason)
			em.description = "I kicked {} like you asked, Darling.".format(user)
		except Exception as e:
			em.description = "I wasn't able to kick {} for you, Darling.\n{}".format(user,e)

		await context.send(embed=em)
		logging.info("{}: kick - {}".format(username,user))

	@commands.command(name="ban",help="Bans a user from the server.")
	@commands.has_permissions(ban_members=True)
	@commands.bot_has_permissions(ban_members = True)
	async def _ban(self,context,user: discord.Member=None, *reason):
		server = context.message.guild
		username = context.message.author
		em = discord.Embed()
		em.title = "Ban {}".format(user)
		if reason:
			reason = ' '.join(reason)
		else: reason = None
		if not isinstance(user,int):
			if not isinstance(user.name.partition("#")[1],int):
				em.description = "You need to be more specific than that, Darling!"
			else: user = server.get_member_named(user)
	
		try:
			await user.ban(reason=reason)
			em.description = "I banned {} like you asked, Darling.".format(user)
		except Exception as e:
			em.description = "I wasn't able to ban {} for you, Darling.\n{}".format(user,e)

		await context.send(embed=em)
		logging.info("{}: ban - {}".format(username,user))

	@commands.command(name="unban",help="Unbans the provided user ID.")
	@commands.has_permissions(ban_members = True)
	@commands.bot_has_permissions(ban_members = True)
	async def _unban(self, context, user: int=None, *reason):
		bans = await context.guild.bans()
		print(bans)
		trueBans = {}
		for ban in bans:
			trueBans[str(ban.user.id)] = ban.user
		if user is not None:
			if reason:
				reason = ' '.join(reason)
			else: reason = None
			await context.guild.unban(trueBans[str(user)])
			await context.send("**:white_check_mark:** <@{}> unbanned.".format(user))
		else:
			await context.send('**:no_entry:** No user provided!')

	@commands.command(name="bans",help="List bans for the server.")
	@commands.has_permissions(ban_members = True)
	@commands.bot_has_permissions(ban_members = True)
	async def _bans(self, ctx):
		users = await ctx.guild.bans()
		if len(users) > 0:
			msg = '`{"ID":21}{"Name":25} Reason\n'
			for entry in users:
				userID = entry.user.id
				userName = str(entry.user)
				if entry.user.bot:
					username = '🤖' + userName #:robot: emoji
				reason = str(entry.reason) #Could be None
				msg += "{userID:<21}{userName:25} {reason}\n"
			embed = discord.Embed(color=0xe74c3c) #Red
			embed.set_thumbnail(url=ctx.guild.icon_url)
			embed.set_footer(text="Server: {ctx.guild.name}")
			embed.add_field(name='Ranks', value=msg + '`', inline=True)
			await ctx.send(embed=embed)
		else:
			await ctx.send('**:negative_squared_cross_mark:** There are no banned users!')

def setup(bot):
	bot.add_cog(Admin(bot))
