import discord,logging,datetime,sys
from discord.ext import commands

class Owner:
	def __init__(self, bot):
		self.bot = bot

	@commands.command(name="restart",help="Restarts Zero Two.")
	@commands.is_owner()
	async def _restart(self, context):
		await context.send('**:ok:** Restarting!')
		self.bot.logout()
		sys.exit(6)

def setup(bot):
	bot.add_cog(Owner(bot))
