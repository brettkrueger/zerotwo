from config.settings import settings
import discord,requests,logging,re,socket
from discord.ext import commands
from urllib.parse import urlencode

logging.basicConfig(filename="002.logs",level=logging.INFO,format='%(asctime)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

class Search():
	def __init__(self,bot):
		self.bot = bot

	@commands.command(name="userinfo",help="Gathers info on tags/provided user IDs.")
	async def _info(self,context):
		mentions = context.message.mentions
		username = context.message.author
		channel = context.message.channel
#		server = self.bot.get_guild(settings["server"])
		server = context.message.guild
		if len(mentions) == 0 :
			search = context.message.content.strip("{}userinfo".format(settings["prefix"]))
		else:
			search = [mention.id for mention in mentions]
			search = search[0]
		info = server.get_member(int(search))
		em = discord.Embed()
		em.title="User Info: {}".format(info)
		em.add_field(name="User",value="Username: {}\nDisplay Name: {}\nNick: {}\nID: {}".format(info,info.display_name,info.nick,info.id))
		em.add_field(name="Joined at",value=info.joined_at)
		em.add_field(name="Roles",value=[role.name for role in info.roles])
		em.set_thumbnail(url=info.avatar_url)
		await channel.send(embed=em)

	@commands.command(pass_context=True,name="animu",help="Checks against Kitsu anime.")
	async def _animu(self,context):
		username = context.message.author
		search = ' '.join(context.message.content.split("{}animu".format(settings["prefix"])))
		url = "https://kitsu.io/api/edge"
		response = requests.get(url+"/anime?filter[text]={}".format(search.replace(' ','%20'))).json()
		em = discord.Embed()
		em.title = search.upper()
		data = response["data"][0]["attributes"]
		em.description = '''
**__Title__**
{} / {}

**__Rating__**
{}

**__Status__** 
{}

**__Start Date__** {}
**__End Date__** {}

**__Summary__**
{}
'''.format(data["titles"]["en_jp"],data["titles"]["en"],data["popularityRank"],data["status"].upper(),data["startDate"],data["endDate"],data["synopsis"])
		em.set_thumbnail(url=data["posterImage"]["small"])
		await context.send(embed=em)
		logging.info("{}: animu - {} / {}".format(username,data["titles"]["en_jp"],data["titles"]["en"]))

	@commands.command(name="host",help="Grab info on IP/host using Shodan's API.")
	async def _host(self,context,ip):
		API=settings["shodan"]
		username = context.message.author
		search = ip
		if not re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",search):
			try: search=socket.gethostbyname(ip)
			except Exception as err:
				print("Error: "+str(err))
				search = None
		response=requests.get("https://api.shodan.io/shodan/host/{}?key={}".format(search, API)).json()
		em = discord.Embed()
		em.title = ip
		try:
			ip = response["ip_str"]
		except: ip = None
		try:
			cc = response["country_code"]
		except: cc = None
		try:
			region = response["region_code"]
		except: region = None
		try:
			hostname = response["hostnames"][0]
		except: hostname = ip
		try:
			if response["org"]:
				company = response["org"]
			else:
				company = reponse["isp"]
		except: company = None
		try:
			ports = response["ports"]
		except: ports = None
#		em.set_image(url="https://i.imgur.com/I1awQwv.jpg")
		em.description = '''
Here's that information you asked for, Darling.

{} - {} - {}
Hostname: {}
Company: {}
Ports: {}
'''.format(ip,cc,region,hostname,company,ports)
		await context.send(embed=em)
		logging.info("{}: geoip - {}".format(username,search))

	@commands.command(pass_context=True,name="wiki",help="Search Wikipedia.")
	async def _wiki(self,context):
		username = context.message.author
		search = ' '.join(context.message.content.split("{}wiki".format(settings["prefix"])))
		response = requests.get("https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch={}&format=json&formatversion=2".format(search)).json()
		data = response["query"]["search"][0]
		em = discord.Embed()
		em.title = data["title"]
		em.color = 0x000000
		url = data["title"].replace(' ','_')
		em.url = "https://en.wikipedia.org/wiki/{}".format(url)
		em.description = '''
{}
[Read more...](https://en.wikipedia.org/wiki/{})
'''.format(data["snippet"].replace('<span class="searchmatch">','').replace('</span>',''),url.replace("(","\(").replace(")","\)"))
		em.set_footer(text=data["title"])
		await context.send(embed=em)
		logging.info("{}: wiki - {}".format(username,search))

	@commands.command(name="pokemon",help="Search Pokemon sprites.")
	async def _pokemon(self,context):
		username = context.message.author
		search = ''.join(context.message.content.split("{}pokemon".format(settings["prefix"]))).lstrip(" ").lstrip(".")
		searchnew = search.replace(" ","_")
		em = discord.Embed()
		if "SHINY" in search.upper():
			search = search.replace("shiny","").lstrip(" ")
			url = "https://raw.githubusercontent.com/msikma/pokesprite/master/icons/pokemon/shiny/"
			em.color = discord.Color.gold()
		else: 
			url = "https://raw.githubusercontent.com/msikma/pokesprite/master/icons/pokemon/regular/"
			em.color = discord.Color.green()
		em.description = search.upper()
		em.set_image(url="{}{}.png".format(url,search.lower()))
		await context.send(embed=em)

def setup(bot):
	bot.add_cog(Search(bot))
