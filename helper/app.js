const config = require("./config/settings.json");
const today = new Date();
const now = new Date().toLocaleTimeString();
const hour = new Date().getHours();
const minute = new Date().getMinutes();

const Discord = require("discord.js");
const Parser = require('rss-parser');

const client = new Discord.Client();
const parser = new Parser();

var newEps = [];

client.on("ready", () => {
	console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`); 

	function newEpisodes(client){
		setInterval(function(){
			(async () => {
				let feed = await parser.parseURL('https://feeds.feedburner.com/crunchyroll/rss/anime?format=xml');
				const embed = new Discord.RichEmbed()
					.setTitle("New Episode!!")
					.setColor("RANDOM")
				var rItems = feed.items.slice(0,1);
				rItems.forEach((item,index) => {
					if (newEps.indexOf(item.link) !== -1){
						return;
					}
					newEps.push(item.link);

					if(item.enclosure){
						embed.setTitle(item.title.split('-')[0])
						embed.setURL(item.enclosure.guid)
						embed.setDescription("New episode!")
						embed.setThumbnail(item.enclosure.url)
						embed.addField(item.title.split('-')[1],item.title.split('-')[2])
						embed.addField("Air Date",item.pubDate)
					} else {
						embed.setTitle(item.title.split('-')[0],item.guid)
						embed.setURL(item.guid)
						embed.setDescription("New episode!")
						embed.addField(item.title.split('-')[1],item.title.split('-')[2])
						embed.addField("Air Date",item.pubDate)
					}
					client.channels.get(config.channels["new-episodes"]).send({embed});
				})
			})();
		}, 3600);
	}

	newEpisodes(client);

});

client.on("message", async message => {
	if(message.author.bot) return;
	if(message.content.indexOf(config.prefix) !== 0) return;
	const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
	const command = args.shift().toLowerCase();
	
	if(command === "ping") {
		const m = await message.channel.send("Ping?");
		m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
	}


});

client.login(config.token);
