import discord,datetime,asyncio
from discord.ext import commands
from config.settings import settings

description = '''
Discord bot for doing stuffs. Not your waifu. Shirley's younger sister.
'''
def getPrefix(bot, message):
	# https://gist.githubusercontent.com/EvieePy/d78c061a4798ae81be9825468fe146be/raw/c0dfece20a5edb7c7f4a6c7e7fac2e242e0e1c79/bot_example.py
	prefixes = [settings["prefix"]]

	if not message.guild:
		return settings["prefix"]

	return commands.when_mentioned_or(*prefixes)(bot, message)

bot = commands.Bot(command_prefix=getPrefix, description=description)

async def status(bot):
	serverCount = len(bot.guilds)
	await bot.change_presence(game=discord.Game(name="02;help || {} guild(s)".format(serverCount)))
	await asyncio.sleep(60)
	await bot.change_presence(game=discord.Game(name="02;invite || Invite me!".format(serverCount)))
	await asyncio.sleep(15)

@bot.event
async def on_ready():
	print('Logged in as {} - {}'.format(bot.user.name,bot.user.id))
	print('------')
	if not hasattr(bot, 'uptime'):
		bot.uptime = datetime.datetime.utcnow()
	while not bot.is_closed():
		await bot.loop.create_task(status(bot))

#@bot.event
#async def on_message(context):
#	if "(╯°□°）╯︵ ┻━┻" in context.content:
#		await bot.send_message(destination=context.channel,content="┬─┬ ノ( ゜-゜ノ)")
#
#	await bot.process_commands(context)

if __name__ == "__main__":
	for extension in settings["extensions"]:
#		try:
		bot.load_extension(extension)
#		except Exception as e:
#			exc = '{}: {}'.format(type(e).__name__, e)
#			print('Failed to load extension {}\n{}'.format(extension, exc))

	bot.run(settings["discord"])
